$( document ).ready(function() {
  $('.owl-carousel').owlCarousel({
      loop: false,
      items: 2,
      margin: -20,
      stagePadding: 15,
      nav: false,
  });
});
